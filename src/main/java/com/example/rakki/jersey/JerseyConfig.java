package com.example.rakki.jersey;

import org.glassfish.jersey.server.ResourceConfig;

//http://localhost:8080/users to load this response
//@Component //Enable this to make jersey to start work
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(UserResource.class);
    }
}
