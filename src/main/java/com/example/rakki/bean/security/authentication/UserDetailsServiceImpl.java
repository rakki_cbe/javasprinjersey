package com.example.rakki.bean.security.authentication;

import com.example.rakki.bean.ServiceForUser;
import com.example.rakki.jersey.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class which gives the user based on the user name it receives
 *
 */
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private ServiceForUser repo;
    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repo.getUserBasedOnUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found.");
        }
         return new LoginUserDetails(user);
    }
}
