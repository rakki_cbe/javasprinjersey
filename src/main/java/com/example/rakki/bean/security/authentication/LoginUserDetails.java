package com.example.rakki.bean.security.authentication;

import com.example.rakki.jersey.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Expected object by the authentication frame work
 * in this it will check the username and password with user name and password send via http or jsp request
 */
public class LoginUserDetails implements UserDetails {
    private User user;
    public LoginUserDetails(User user) {
        this.user = user;
    }

    /**
     * This is resposible for the roles user has. IT will helpfull when u give certain api access to specific used role
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<SimpleGrantedAuthority> list = new ArrayList<>();
        list.add( new SimpleGrantedAuthority("USER"));
        return list;
        //user.getAuthorities().stream().map(authority -> new SimpleGrantedAuthority(authority.getName().toString())).collect(Collectors.toList());
    }
    public int getId() {
        return user.getId();
    }

    /**
     * THis {noop} is ecryption type we use in our database for password
     * noop means no encryption
     * {bcrypt} bcrypt encryption
     * @return
     */
    @Override
    public String getPassword() {
        return "{noop}"+user.getPassword();
    }
    @Override
    public String getUsername() {
        return user.getUserName();
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
    public User getUserDetails() {
        return user;
    }
}
