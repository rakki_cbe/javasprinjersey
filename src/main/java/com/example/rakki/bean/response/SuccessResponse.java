package com.example.rakki.bean.response;

import java.io.Serializable;

public class SuccessResponse<T> extends BaseResponse implements Serializable {
    public SuccessResponse() {
        responseCode = "1111";
        responseMsg="Success";
    }
   public  T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
