package com.example.rakki.bean.response;

import java.io.Serializable;

public class FailureResponse extends  BaseResponse implements Serializable {
    public FailureResponse(String errorCode,String errorMsg) {
        responseCode = errorCode;
        responseMsg = errorMsg;
    }
}
