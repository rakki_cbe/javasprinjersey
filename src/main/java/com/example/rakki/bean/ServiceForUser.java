package com.example.rakki.bean;

import com.example.rakki.jersey.User;
import com.example.rakki.jersey.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceForUser {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public Users getUsers() {
        String sql = "SELECT * FROM user";
        List<User> user = jdbcTemplate.query(sql,
                BeanPropertyRowMapper.newInstance(User.class));

        Users users = new Users();
        users.setUsers(new ArrayList<>(user));
        return users;
    }

    public int addUser(User user) {
        String sql = "INSERT INTO user (name, userName, password) VALUES (?, ?, ?)";
        return jdbcTemplate.update(sql, user.getName(), user.getUserName(), user.getPassword());
    }

    public User getUserBasedOnUserName(String username) {
        String sql = "SELECT * FROM user WHERE userName=?";
        List<User> user = jdbcTemplate.query(sql,new String[] {username},
                BeanPropertyRowMapper.newInstance(User.class));
        if(user.size()>0) {
            return user.get(0);
        } else{
            return null;
        }
    }


    private Map<Integer, User> DB = new HashMap<>();

    public ServiceForUser() {
        User user1 = new User();
        user1.setId(1);
        user1.setName("John");
        user1.setPassword("Wick");
        user1.setUserName("john_username");

        User user2 = new User();
        user2.setId(2);
        user1.setName("Harry");
        user1.setPassword("parter");
        user1.setUserName("harry_username");

        DB.put(user1.getId(), user1);
        DB.put(user2.getId(), user2);
    }



    public java.util.Collection<User> getUserArray() {
        return DB.values();
    }



    public User getUserBasedOnId(int id) {
        return DB.get(id);
    }

    public void updateUser(User user, int id) {
        User temp = DB.get(id);
        temp.setUserName(user.getUserName());
        temp.setPassword(user.getPassword());
        DB.put(temp.getId(), temp);
    }

    public void removeUser(User user) {
        if (DB.containsValue(user)) {
            DB.remove(user);
        }
    }
}
