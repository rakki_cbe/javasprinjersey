package com.example.rakki.bean;

import com.example.rakki.jersey.User;
import com.example.rakki.jersey.Users;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(value = "com.example.rakki.jersey")
public class MyConfiguration {
    @Bean
    /*
       singleton ,prototype,request,session,global-session
       are the scope we can use
     */
    @Scope(value = "prototype")
    public User getUser() {
        return new User();
    }

    @Bean
    @Scope(value = "prototype")
    public Users getUsers() {
        return new Users();
    }

    @Bean
    @Scope(value = "singleton")
    public ServiceForUser getUserService() {
        return new ServiceForUser();
    }
}
