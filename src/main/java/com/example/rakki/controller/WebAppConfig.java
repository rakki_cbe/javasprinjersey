package com.example.rakki.controller;

import com.example.rakki.jersey.User;
import com.example.rakki.jersey.Users;
import com.example.rakki.bean.ServiceForUser;
import com.example.rakki.bean.response.BaseResponse;
import com.example.rakki.bean.response.FailureResponse;
import com.example.rakki.bean.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//http://localhost:8080/users to load this response
@RestController
public class WebAppConfig {
    @Autowired
    ServiceForUser repo;

    @GetMapping("/users")
    public BaseResponse getUsers() {

       SuccessResponse response= new SuccessResponse();
        response.setData(repo.getUsers());
        return response;
    }

    @PostMapping("/createuser")
    public BaseResponse createUser(@RequestBody User newUser){
        int result=repo.addUser(newUser);
       if (result > 0) {
			return  new SuccessResponse<String>();
		} else
        {
            return  new FailureResponse("5555","Something went wrong");
        }
    }
}
